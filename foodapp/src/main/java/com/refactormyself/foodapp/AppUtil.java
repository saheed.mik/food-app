package com.refactormyself.foodapp;

import android.app.Application;

import com.refactormyself.foodapp.model.AppDatabase;

public class AppUtil extends Application {

//    private AppExecutors mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();

//        mAppExecutors = new AppExecutors();
    }

//    public AppDatabase getDatabase() {
//        return AppDatabase.getInstance(this, mAppExecutors);
//    }

    public AppDataRepository getRepository() {
        return AppDataRepository.getInstance(this);
    }
}